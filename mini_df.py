import sys
import getopt
import os
import shutil

def get_folder_size(start_folder_path):
    total_folder_size = 0
    for dirpath, dirnames, filenames in os.walk(start_folder_path):
        for element in filenames:
            filepath = os.path.join(dirpath, element)
            if not os.path.islink(filepath):
                total_folder_size = total_folder_size + os.path.getsize(filepath)
    return total_folder_size

def space_usage(paths):
	stats = []
	sizes = []
	for path in paths:
		try:
			stat = shutil.disk_usage(path)
		except FileNotFoundError:
			print(f"File not found error for {path}") 
			sys.exit()
		if os.path.isdir(path):
			size = get_folder_size(path) #size of folder and others subfolders
		else:
			size = os.path.getsize(path) #size of file
		stats.append(stat)
		sizes.append(size)	
	return stats, sizes	
	
def bytes_to_MB(in_bytes):
	inMB = in_bytes * 0.000001
	inMB = round(inMB,3) # Round to less crazy number of decimal points.
	return inMB

def main(argv):
	opts=[]
	
	try:
		options, arguments = getopt.getopt(argv,"-h")
	except getopt.GetoptError:
		print('Unknown option argument')
		sys.exit()
	for opt,arg in options:		#extract options from key:value tupple
		opts.append(opt)
	
	if len(arguments) == 0:
		arguments.append(os.getcwd())
	
	if "-h" in opts:
		stats, folders_size = space_usage(arguments)
		for x in range(len(stats)):
			print(f"Space information for path: {arguments[x]} Folder/file size:{bytes_to_MB(folders_size[x])}MB, On disk: Total:{bytes_to_MB(stats[x][0])}MB, Free:{bytes_to_MB(stats[x][2])}MB, Used:{bytes_to_MB(stats[x][1])}MB")
	else:
		stats, folders_size = space_usage(arguments)
		for x in range(len(stats)):
			print(str(stats[x])+ ", Folder/file size=" + str(folders_size[x]))
   	
if __name__ == "__main__":
   main(sys.argv[1:])



